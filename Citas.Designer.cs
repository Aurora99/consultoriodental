﻿namespace ConsultorioDental
{
    partial class Citas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Citas));
            this.pBCitas = new System.Windows.Forms.PictureBox();
            this.lblSerPro = new System.Windows.Forms.Label();
            this.lblIDProSer = new System.Windows.Forms.Label();
            this.lblCosto = new System.Windows.Forms.Label();
            this.txtBProSer = new System.Windows.Forms.TextBox();
            this.txtBIDProSer = new System.Windows.Forms.TextBox();
            this.txtBCosto = new System.Windows.Forms.TextBox();
            this.lblNombre = new System.Windows.Forms.Label();
            this.lblTelefono = new System.Windows.Forms.Label();
            this.txtBNombre = new System.Windows.Forms.TextBox();
            this.txtBTelefono = new System.Windows.Forms.TextBox();
            this.lblIdCliente = new System.Windows.Forms.Label();
            this.txtBIdCliente = new System.Windows.Forms.TextBox();
            this.btnGuardarClientes = new System.Windows.Forms.Button();
            this.btnGuardarPS = new System.Windows.Forms.Button();
            this.lblApellidos = new System.Windows.Forms.Label();
            this.txtBApellidos = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pBCitas)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSalir
            // 
            this.btnSalir.Location = new System.Drawing.Point(718, 504);
            // 
            // pBCitas
            // 
            this.pBCitas.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pBCitas.BackgroundImage")));
            this.pBCitas.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pBCitas.Location = new System.Drawing.Point(510, 27);
            this.pBCitas.Name = "pBCitas";
            this.pBCitas.Size = new System.Drawing.Size(287, 166);
            this.pBCitas.TabIndex = 1;
            this.pBCitas.TabStop = false;
            // 
            // lblSerPro
            // 
            this.lblSerPro.AutoSize = true;
            this.lblSerPro.Location = new System.Drawing.Point(36, 333);
            this.lblSerPro.Name = "lblSerPro";
            this.lblSerPro.Size = new System.Drawing.Size(96, 13);
            this.lblSerPro.TabIndex = 2;
            this.lblSerPro.Text = "Servicio/Producto:";
            // 
            // lblIDProSer
            // 
            this.lblIDProSer.AutoSize = true;
            this.lblIDProSer.Location = new System.Drawing.Point(36, 311);
            this.lblIDProSer.Name = "lblIDProSer";
            this.lblIDProSer.Size = new System.Drawing.Size(117, 13);
            this.lblIDProSer.TabIndex = 3;
            this.lblIDProSer.Text = "Id_Producto / Servicio:";
            // 
            // lblCosto
            // 
            this.lblCosto.AutoSize = true;
            this.lblCosto.Location = new System.Drawing.Point(36, 367);
            this.lblCosto.Name = "lblCosto";
            this.lblCosto.Size = new System.Drawing.Size(37, 13);
            this.lblCosto.TabIndex = 4;
            this.lblCosto.Text = "Costo:";
            // 
            // txtBProSer
            // 
            this.txtBProSer.Location = new System.Drawing.Point(176, 330);
            this.txtBProSer.Name = "txtBProSer";
            this.txtBProSer.Size = new System.Drawing.Size(241, 20);
            this.txtBProSer.TabIndex = 5;
            // 
            // txtBIDProSer
            // 
            this.txtBIDProSer.Location = new System.Drawing.Point(176, 304);
            this.txtBIDProSer.Name = "txtBIDProSer";
            this.txtBIDProSer.Size = new System.Drawing.Size(241, 20);
            this.txtBIDProSer.TabIndex = 6;
            this.txtBIDProSer.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // txtBCosto
            // 
            this.txtBCosto.Location = new System.Drawing.Point(176, 360);
            this.txtBCosto.Name = "txtBCosto";
            this.txtBCosto.Size = new System.Drawing.Size(241, 20);
            this.txtBCosto.TabIndex = 7;
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.Location = new System.Drawing.Point(31, 100);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(47, 13);
            this.lblNombre.TabIndex = 8;
            this.lblNombre.Text = "Nombre:";
            // 
            // lblTelefono
            // 
            this.lblTelefono.AutoSize = true;
            this.lblTelefono.Location = new System.Drawing.Point(31, 162);
            this.lblTelefono.Name = "lblTelefono";
            this.lblTelefono.Size = new System.Drawing.Size(52, 13);
            this.lblTelefono.TabIndex = 9;
            this.lblTelefono.Text = "Telefono:";
            // 
            // txtBNombre
            // 
            this.txtBNombre.Location = new System.Drawing.Point(156, 93);
            this.txtBNombre.Name = "txtBNombre";
            this.txtBNombre.Size = new System.Drawing.Size(241, 20);
            this.txtBNombre.TabIndex = 10;
            // 
            // txtBTelefono
            // 
            this.txtBTelefono.Location = new System.Drawing.Point(156, 155);
            this.txtBTelefono.Name = "txtBTelefono";
            this.txtBTelefono.Size = new System.Drawing.Size(241, 20);
            this.txtBTelefono.TabIndex = 11;
            // 
            // lblIdCliente
            // 
            this.lblIdCliente.AutoSize = true;
            this.lblIdCliente.Location = new System.Drawing.Point(31, 66);
            this.lblIdCliente.Name = "lblIdCliente";
            this.lblIdCliente.Size = new System.Drawing.Size(57, 13);
            this.lblIdCliente.TabIndex = 12;
            this.lblIdCliente.Text = "Id_Cliente:";
            // 
            // txtBIdCliente
            // 
            this.txtBIdCliente.Location = new System.Drawing.Point(156, 59);
            this.txtBIdCliente.Name = "txtBIdCliente";
            this.txtBIdCliente.Size = new System.Drawing.Size(151, 20);
            this.txtBIdCliente.TabIndex = 13;
            this.txtBIdCliente.TextChanged += new System.EventHandler(this.txtBIdCliente_TextChanged);
            // 
            // btnGuardarClientes
            // 
            this.btnGuardarClientes.Location = new System.Drawing.Point(156, 181);
            this.btnGuardarClientes.Name = "btnGuardarClientes";
            this.btnGuardarClientes.Size = new System.Drawing.Size(75, 23);
            this.btnGuardarClientes.TabIndex = 14;
            this.btnGuardarClientes.Text = "Guardar";
            this.btnGuardarClientes.UseVisualStyleBackColor = true;
            this.btnGuardarClientes.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // btnGuardarPS
            // 
            this.btnGuardarPS.Location = new System.Drawing.Point(342, 401);
            this.btnGuardarPS.Name = "btnGuardarPS";
            this.btnGuardarPS.Size = new System.Drawing.Size(75, 23);
            this.btnGuardarPS.TabIndex = 15;
            this.btnGuardarPS.Text = "Guardar";
            this.btnGuardarPS.UseVisualStyleBackColor = true;
            this.btnGuardarPS.Click += new System.EventHandler(this.btnGuardarPS_Click);
            // 
            // lblApellidos
            // 
            this.lblApellidos.AutoSize = true;
            this.lblApellidos.Location = new System.Drawing.Point(34, 129);
            this.lblApellidos.Name = "lblApellidos";
            this.lblApellidos.Size = new System.Drawing.Size(52, 13);
            this.lblApellidos.TabIndex = 16;
            this.lblApellidos.Text = "Apellidos:";
            // 
            // txtBApellidos
            // 
            this.txtBApellidos.Location = new System.Drawing.Point(156, 121);
            this.txtBApellidos.Name = "txtBApellidos";
            this.txtBApellidos.Size = new System.Drawing.Size(241, 20);
            this.txtBApellidos.TabIndex = 17;
            // 
            // Citas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(850, 569);
            this.Controls.Add(this.txtBApellidos);
            this.Controls.Add(this.lblApellidos);
            this.Controls.Add(this.btnGuardarPS);
            this.Controls.Add(this.btnGuardarClientes);
            this.Controls.Add(this.txtBIdCliente);
            this.Controls.Add(this.lblIdCliente);
            this.Controls.Add(this.txtBTelefono);
            this.Controls.Add(this.txtBNombre);
            this.Controls.Add(this.lblTelefono);
            this.Controls.Add(this.lblNombre);
            this.Controls.Add(this.txtBCosto);
            this.Controls.Add(this.txtBIDProSer);
            this.Controls.Add(this.txtBProSer);
            this.Controls.Add(this.lblCosto);
            this.Controls.Add(this.lblIDProSer);
            this.Controls.Add(this.lblSerPro);
            this.Controls.Add(this.pBCitas);
            this.Name = "Citas";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Citas";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Citas_FormClosed);
            this.Load += new System.EventHandler(this.Citas_Load);
            this.Controls.SetChildIndex(this.btnSalir, 0);
            this.Controls.SetChildIndex(this.pBCitas, 0);
            this.Controls.SetChildIndex(this.lblSerPro, 0);
            this.Controls.SetChildIndex(this.lblIDProSer, 0);
            this.Controls.SetChildIndex(this.lblCosto, 0);
            this.Controls.SetChildIndex(this.txtBProSer, 0);
            this.Controls.SetChildIndex(this.txtBIDProSer, 0);
            this.Controls.SetChildIndex(this.txtBCosto, 0);
            this.Controls.SetChildIndex(this.lblNombre, 0);
            this.Controls.SetChildIndex(this.lblTelefono, 0);
            this.Controls.SetChildIndex(this.txtBNombre, 0);
            this.Controls.SetChildIndex(this.txtBTelefono, 0);
            this.Controls.SetChildIndex(this.lblIdCliente, 0);
            this.Controls.SetChildIndex(this.txtBIdCliente, 0);
            this.Controls.SetChildIndex(this.btnGuardarClientes, 0);
            this.Controls.SetChildIndex(this.btnGuardarPS, 0);
            this.Controls.SetChildIndex(this.lblApellidos, 0);
            this.Controls.SetChildIndex(this.txtBApellidos, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pBCitas)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pBCitas;
        private System.Windows.Forms.Label lblSerPro;
        private System.Windows.Forms.Label lblIDProSer;
        private System.Windows.Forms.Label lblCosto;
        private System.Windows.Forms.TextBox txtBProSer;
        private System.Windows.Forms.TextBox txtBIDProSer;
        private System.Windows.Forms.TextBox txtBCosto;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.Label lblTelefono;
        private System.Windows.Forms.TextBox txtBNombre;
        private System.Windows.Forms.TextBox txtBTelefono;
        private System.Windows.Forms.Label lblIdCliente;
        private System.Windows.Forms.TextBox txtBIdCliente;
        private System.Windows.Forms.Button btnGuardarClientes;
        private System.Windows.Forms.Button btnGuardarPS;
        private System.Windows.Forms.Label lblApellidos;
        private System.Windows.Forms.TextBox txtBApellidos;
    }
}