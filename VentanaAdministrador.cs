﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MILIBRERIA;

namespace ConsultorioDental
{
    public partial class VentanaAdministrador : FormBase
    {
        public VentanaAdministrador()
        {
            InitializeComponent();
        }

       

        private void VentanaAdministrador_Load(object sender, EventArgs e)
        {
            string cmd = "SELECT * FROM Usuario WHERE Id_Usuario=" + Inicio.Codigo;

            DataSet DS = UTILIDADES.Iniciar(cmd);

            lblTAdministrador.Text = DS.Tables[0].Rows[0]["Nom_Usuario"].ToString();
            lblTUsuario.Text = DS.Tables[0].Rows[0]["account"].ToString();
            lblTCodigo.Text = DS.Tables[0].Rows[0]["Id_Usuario"].ToString();
        }


        private void VentanaAdministrador_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void btnPaginaP_Click(object sender, EventArgs e)
        {
            PaginaPrincipal paginaPrincipal = new PaginaPrincipal();
            paginaPrincipal.Show();
        }

        private void btnCitas_Click(object sender, EventArgs e)
        {
            Citas citas = new Citas();
            citas.Show();
        }
    }
}
