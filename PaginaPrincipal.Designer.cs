﻿namespace ConsultorioDental
{
    partial class PaginaPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PaginaPrincipal));
            this.lblConsultorio = new System.Windows.Forms.Label();
            this.pcBExtraccion = new System.Windows.Forms.PictureBox();
            this.pcBLimpieza = new System.Windows.Forms.PictureBox();
            this.pcBProductos = new System.Windows.Forms.PictureBox();
            this.pcBOrtodoncia = new System.Windows.Forms.PictureBox();
            this.btnExtraciones = new System.Windows.Forms.Button();
            this.btnLimpieza = new System.Windows.Forms.Button();
            this.btnOrtodoncia = new System.Windows.Forms.Button();
            this.btnProductos = new System.Windows.Forms.Button();
            this.btnCitas = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pcBExtraccion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcBLimpieza)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcBProductos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcBOrtodoncia)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSalir
            // 
            this.btnSalir.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSalir.Location = new System.Drawing.Point(59, 530);
            this.btnSalir.UseVisualStyleBackColor = false;
            // 
            // lblConsultorio
            // 
            this.lblConsultorio.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.lblConsultorio.Font = new System.Drawing.Font("Times New Roman", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConsultorio.ForeColor = System.Drawing.Color.PaleVioletRed;
            this.lblConsultorio.Location = new System.Drawing.Point(104, 9);
            this.lblConsultorio.Name = "lblConsultorio";
            this.lblConsultorio.Size = new System.Drawing.Size(599, 50);
            this.lblConsultorio.TabIndex = 0;
            this.lblConsultorio.Text = "CONSULTORIO DENTAL";
            // 
            // pcBExtraccion
            // 
            this.pcBExtraccion.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pcBExtraccion.BackgroundImage")));
            this.pcBExtraccion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pcBExtraccion.Location = new System.Drawing.Point(144, 96);
            this.pcBExtraccion.Name = "pcBExtraccion";
            this.pcBExtraccion.Size = new System.Drawing.Size(177, 112);
            this.pcBExtraccion.TabIndex = 1;
            this.pcBExtraccion.TabStop = false;
            // 
            // pcBLimpieza
            // 
            this.pcBLimpieza.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pcBLimpieza.BackgroundImage")));
            this.pcBLimpieza.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pcBLimpieza.Location = new System.Drawing.Point(446, 96);
            this.pcBLimpieza.Name = "pcBLimpieza";
            this.pcBLimpieza.Size = new System.Drawing.Size(204, 112);
            this.pcBLimpieza.TabIndex = 2;
            this.pcBLimpieza.TabStop = false;
            // 
            // pcBProductos
            // 
            this.pcBProductos.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pcBProductos.BackgroundImage")));
            this.pcBProductos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pcBProductos.Location = new System.Drawing.Point(457, 319);
            this.pcBProductos.Name = "pcBProductos";
            this.pcBProductos.Size = new System.Drawing.Size(172, 126);
            this.pcBProductos.TabIndex = 3;
            this.pcBProductos.TabStop = false;
            // 
            // pcBOrtodoncia
            // 
            this.pcBOrtodoncia.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pcBOrtodoncia.BackgroundImage")));
            this.pcBOrtodoncia.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pcBOrtodoncia.Location = new System.Drawing.Point(144, 319);
            this.pcBOrtodoncia.Name = "pcBOrtodoncia";
            this.pcBOrtodoncia.Size = new System.Drawing.Size(177, 126);
            this.pcBOrtodoncia.TabIndex = 4;
            this.pcBOrtodoncia.TabStop = false;
            // 
            // btnExtraciones
            // 
            this.btnExtraciones.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnExtraciones.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExtraciones.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExtraciones.ForeColor = System.Drawing.Color.HotPink;
            this.btnExtraciones.Location = new System.Drawing.Point(164, 231);
            this.btnExtraciones.Name = "btnExtraciones";
            this.btnExtraciones.Size = new System.Drawing.Size(136, 33);
            this.btnExtraciones.TabIndex = 5;
            this.btnExtraciones.Text = "Extracciones";
            this.btnExtraciones.UseVisualStyleBackColor = false;
            this.btnExtraciones.Click += new System.EventHandler(this.btnExtraciones_Click);
            // 
            // btnLimpieza
            // 
            this.btnLimpieza.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnLimpieza.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLimpieza.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLimpieza.ForeColor = System.Drawing.Color.HotPink;
            this.btnLimpieza.Location = new System.Drawing.Point(494, 231);
            this.btnLimpieza.Name = "btnLimpieza";
            this.btnLimpieza.Size = new System.Drawing.Size(102, 33);
            this.btnLimpieza.TabIndex = 6;
            this.btnLimpieza.Text = "Limpieza";
            this.btnLimpieza.UseVisualStyleBackColor = false;
            this.btnLimpieza.Click += new System.EventHandler(this.btnLimpieza_Click);
            // 
            // btnOrtodoncia
            // 
            this.btnOrtodoncia.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnOrtodoncia.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOrtodoncia.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOrtodoncia.ForeColor = System.Drawing.Color.HotPink;
            this.btnOrtodoncia.Location = new System.Drawing.Point(164, 465);
            this.btnOrtodoncia.Name = "btnOrtodoncia";
            this.btnOrtodoncia.Size = new System.Drawing.Size(119, 33);
            this.btnOrtodoncia.TabIndex = 7;
            this.btnOrtodoncia.Text = "Ortodoncia";
            this.btnOrtodoncia.UseVisualStyleBackColor = false;
            this.btnOrtodoncia.Click += new System.EventHandler(this.btnOrtodoncia_Click);
            // 
            // btnProductos
            // 
            this.btnProductos.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnProductos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProductos.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProductos.ForeColor = System.Drawing.Color.HotPink;
            this.btnProductos.Location = new System.Drawing.Point(494, 465);
            this.btnProductos.Name = "btnProductos";
            this.btnProductos.Size = new System.Drawing.Size(102, 33);
            this.btnProductos.TabIndex = 8;
            this.btnProductos.Text = "Productos";
            this.btnProductos.UseVisualStyleBackColor = false;
            this.btnProductos.Click += new System.EventHandler(this.btnProductos_Click);
            // 
            // btnCitas
            // 
            this.btnCitas.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnCitas.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCitas.ForeColor = System.Drawing.Color.HotPink;
            this.btnCitas.Location = new System.Drawing.Point(587, 529);
            this.btnCitas.Name = "btnCitas";
            this.btnCitas.Size = new System.Drawing.Size(107, 37);
            this.btnCitas.TabIndex = 10;
            this.btnCitas.Text = "Citas";
            this.btnCitas.UseVisualStyleBackColor = false;
            this.btnCitas.Click += new System.EventHandler(this.btnCitas_Click);
            // 
            // PaginaPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(747, 583);
            this.Controls.Add(this.btnCitas);
            this.Controls.Add(this.btnProductos);
            this.Controls.Add(this.btnOrtodoncia);
            this.Controls.Add(this.btnLimpieza);
            this.Controls.Add(this.btnExtraciones);
            this.Controls.Add(this.pcBOrtodoncia);
            this.Controls.Add(this.pcBProductos);
            this.Controls.Add(this.pcBLimpieza);
            this.Controls.Add(this.pcBExtraccion);
            this.Controls.Add(this.lblConsultorio);
            this.ForeColor = System.Drawing.Color.HotPink;
            this.Name = "PaginaPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PaginaPrincipal";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.PaginaPrincipal_FormClosed);
            this.Load += new System.EventHandler(this.PaginaPrincipal_Load);
            this.Controls.SetChildIndex(this.lblConsultorio, 0);
            this.Controls.SetChildIndex(this.pcBExtraccion, 0);
            this.Controls.SetChildIndex(this.pcBLimpieza, 0);
            this.Controls.SetChildIndex(this.pcBProductos, 0);
            this.Controls.SetChildIndex(this.pcBOrtodoncia, 0);
            this.Controls.SetChildIndex(this.btnExtraciones, 0);
            this.Controls.SetChildIndex(this.btnLimpieza, 0);
            this.Controls.SetChildIndex(this.btnOrtodoncia, 0);
            this.Controls.SetChildIndex(this.btnProductos, 0);
            this.Controls.SetChildIndex(this.btnCitas, 0);
            this.Controls.SetChildIndex(this.btnSalir, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pcBExtraccion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcBLimpieza)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcBProductos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcBOrtodoncia)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblConsultorio;
        private System.Windows.Forms.PictureBox pcBExtraccion;
        private System.Windows.Forms.PictureBox pcBLimpieza;
        private System.Windows.Forms.PictureBox pcBProductos;
        private System.Windows.Forms.PictureBox pcBOrtodoncia;
        private System.Windows.Forms.Button btnExtraciones;
        private System.Windows.Forms.Button btnLimpieza;
        private System.Windows.Forms.Button btnOrtodoncia;
        private System.Windows.Forms.Button btnProductos;
        private System.Windows.Forms.Button btnCitas;
    }
}