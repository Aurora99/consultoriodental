﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConsultorioDental
{
    public partial class PaginaPrincipal : FormBase
    {
        public PaginaPrincipal()
        {
            InitializeComponent();
        }

        private void PaginaPrincipal_Load(object sender, EventArgs e)
        {

        }

        private void btnCitas_Click(object sender, EventArgs e)
        {
            Citas citas = new Citas();
            citas.Show();
        }

        private void btnExtraciones_Click(object sender, EventArgs e)
        {
            Extracciones extracciones = new Extracciones();
            this.Hide();
            extracciones.Show();
        }

        private void btnLimpieza_Click(object sender, EventArgs e)
        {
            Limpieza limpieza = new Limpieza();
            limpieza.Show();
            this.Hide();
        }

        private void btnOrtodoncia_Click(object sender, EventArgs e)
        {
            Ortodoncía ortodoncia = new Ortodoncía();
            ortodoncia.Show();
            this.Hide();
        }

        private void btnProductos_Click(object sender, EventArgs e)
        {
            Productos productos = new Productos();
            productos.Show();
            this.Hide();
        }

        private void PaginaPrincipal_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}
