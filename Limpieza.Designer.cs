﻿namespace ConsultorioDental
{
    partial class Limpieza
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Limpieza));
            this.btnRegresar = new System.Windows.Forms.Button();
            this.btnCitas = new System.Windows.Forms.Button();
            this.gBEnguaje = new System.Windows.Forms.GroupBox();
            this.lblPrecioE = new System.Windows.Forms.Label();
            this.ckBEnguaje = new System.Windows.Forms.CheckBox();
            this.pBEnguaje = new System.Windows.Forms.PictureBox();
            this.gBBlanqueado = new System.Windows.Forms.GroupBox();
            this.lblPrecioB = new System.Windows.Forms.Label();
            this.ckBBlanqueado = new System.Windows.Forms.CheckBox();
            this.pBBlanqueado = new System.Windows.Forms.PictureBox();
            this.gBEnguaje.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pBEnguaje)).BeginInit();
            this.gBBlanqueado.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pBBlanqueado)).BeginInit();
            this.SuspendLayout();
            // 
            // btnRegresar
            // 
            this.btnRegresar.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRegresar.Location = new System.Drawing.Point(28, 459);
            this.btnRegresar.Name = "btnRegresar";
            this.btnRegresar.Size = new System.Drawing.Size(91, 29);
            this.btnRegresar.TabIndex = 5;
            this.btnRegresar.Text = "Regresar";
            this.btnRegresar.UseVisualStyleBackColor = true;
            this.btnRegresar.Click += new System.EventHandler(this.btnRegresar_Click);
            // 
            // btnCitas
            // 
            this.btnCitas.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCitas.Location = new System.Drawing.Point(617, 459);
            this.btnCitas.Name = "btnCitas";
            this.btnCitas.Size = new System.Drawing.Size(91, 29);
            this.btnCitas.TabIndex = 6;
            this.btnCitas.Text = "Citas";
            this.btnCitas.UseVisualStyleBackColor = true;
            this.btnCitas.Click += new System.EventHandler(this.btnCitas_Click);
            // 
            // gBEnguaje
            // 
            this.gBEnguaje.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.gBEnguaje.Controls.Add(this.lblPrecioE);
            this.gBEnguaje.Controls.Add(this.ckBEnguaje);
            this.gBEnguaje.Controls.Add(this.pBEnguaje);
            this.gBEnguaje.Location = new System.Drawing.Point(72, 41);
            this.gBEnguaje.Name = "gBEnguaje";
            this.gBEnguaje.Size = new System.Drawing.Size(261, 362);
            this.gBEnguaje.TabIndex = 7;
            this.gBEnguaje.TabStop = false;
            // 
            // lblPrecioE
            // 
            this.lblPrecioE.AutoSize = true;
            this.lblPrecioE.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrecioE.Location = new System.Drawing.Point(62, 255);
            this.lblPrecioE.Name = "lblPrecioE";
            this.lblPrecioE.Size = new System.Drawing.Size(87, 19);
            this.lblPrecioE.TabIndex = 2;
            this.lblPrecioE.Text = "Precio: $150";
            // 
            // ckBEnguaje
            // 
            this.ckBEnguaje.AutoSize = true;
            this.ckBEnguaje.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckBEnguaje.Location = new System.Drawing.Point(42, 196);
            this.ckBEnguaje.Name = "ckBEnguaje";
            this.ckBEnguaje.Size = new System.Drawing.Size(157, 21);
            this.ckBEnguaje.TabIndex = 1;
            this.ckBEnguaje.Text = "ENGUAJE BUCAL";
            this.ckBEnguaje.UseVisualStyleBackColor = true;
            // 
            // pBEnguaje
            // 
            this.pBEnguaje.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pBEnguaje.BackgroundImage")));
            this.pBEnguaje.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pBEnguaje.Location = new System.Drawing.Point(42, 43);
            this.pBEnguaje.Name = "pBEnguaje";
            this.pBEnguaje.Size = new System.Drawing.Size(150, 113);
            this.pBEnguaje.TabIndex = 0;
            this.pBEnguaje.TabStop = false;
            // 
            // gBBlanqueado
            // 
            this.gBBlanqueado.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.gBBlanqueado.Controls.Add(this.lblPrecioB);
            this.gBBlanqueado.Controls.Add(this.ckBBlanqueado);
            this.gBBlanqueado.Controls.Add(this.pBBlanqueado);
            this.gBBlanqueado.Location = new System.Drawing.Point(420, 41);
            this.gBBlanqueado.Name = "gBBlanqueado";
            this.gBBlanqueado.Size = new System.Drawing.Size(261, 362);
            this.gBBlanqueado.TabIndex = 8;
            this.gBBlanqueado.TabStop = false;
            // 
            // lblPrecioB
            // 
            this.lblPrecioB.AutoSize = true;
            this.lblPrecioB.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrecioB.Location = new System.Drawing.Point(77, 255);
            this.lblPrecioB.Name = "lblPrecioB";
            this.lblPrecioB.Size = new System.Drawing.Size(87, 19);
            this.lblPrecioB.TabIndex = 3;
            this.lblPrecioB.Text = "Precio: $450";
            // 
            // ckBBlanqueado
            // 
            this.ckBBlanqueado.AutoSize = true;
            this.ckBBlanqueado.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckBBlanqueado.Location = new System.Drawing.Point(58, 196);
            this.ckBBlanqueado.Name = "ckBBlanqueado";
            this.ckBBlanqueado.Size = new System.Drawing.Size(137, 38);
            this.ckBBlanqueado.TabIndex = 2;
            this.ckBBlanqueado.Text = "BLANQUEADO\r\nDE DIENTES\r\n";
            this.ckBBlanqueado.UseVisualStyleBackColor = true;
            this.ckBBlanqueado.CheckedChanged += new System.EventHandler(this.ckBBlanqueado_CheckedChanged);
            // 
            // pBBlanqueado
            // 
            this.pBBlanqueado.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pBBlanqueado.BackgroundImage")));
            this.pBBlanqueado.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pBBlanqueado.Location = new System.Drawing.Point(35, 43);
            this.pBBlanqueado.Name = "pBBlanqueado";
            this.pBBlanqueado.Size = new System.Drawing.Size(178, 124);
            this.pBBlanqueado.TabIndex = 1;
            this.pBBlanqueado.TabStop = false;
            // 
            // Limpieza
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(746, 512);
            this.Controls.Add(this.gBBlanqueado);
            this.Controls.Add(this.gBEnguaje);
            this.Controls.Add(this.btnCitas);
            this.Controls.Add(this.btnRegresar);
            this.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.Name = "Limpieza";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Limpieza";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Limpieza_FormClosed);
            this.gBEnguaje.ResumeLayout(false);
            this.gBEnguaje.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pBEnguaje)).EndInit();
            this.gBBlanqueado.ResumeLayout(false);
            this.gBBlanqueado.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pBBlanqueado)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnRegresar;
        private System.Windows.Forms.Button btnCitas;
        private System.Windows.Forms.GroupBox gBEnguaje;
        private System.Windows.Forms.GroupBox gBBlanqueado;
        private System.Windows.Forms.Label lblPrecioE;
        private System.Windows.Forms.CheckBox ckBEnguaje;
        private System.Windows.Forms.PictureBox pBEnguaje;
        private System.Windows.Forms.Label lblPrecioB;
        private System.Windows.Forms.CheckBox ckBBlanqueado;
        private System.Windows.Forms.PictureBox pBBlanqueado;
    }
}