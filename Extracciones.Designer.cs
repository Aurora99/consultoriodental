﻿namespace ConsultorioDental
{
    partial class Extracciones
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Extracciones));
            this.btnRegresar = new System.Windows.Forms.Button();
            this.btnCitas = new System.Windows.Forms.Button();
            this.gBNiños = new System.Windows.Forms.GroupBox();
            this.pBNiños = new System.Windows.Forms.PictureBox();
            this.lblPrecioN = new System.Windows.Forms.Label();
            this.ckBNiños = new System.Windows.Forms.CheckBox();
            this.gBAdulto = new System.Windows.Forms.GroupBox();
            this.pBAdultos = new System.Windows.Forms.PictureBox();
            this.lblPrecioA = new System.Windows.Forms.Label();
            this.ckBAdultos = new System.Windows.Forms.CheckBox();
            this.gBNiños.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pBNiños)).BeginInit();
            this.gBAdulto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pBAdultos)).BeginInit();
            this.SuspendLayout();
            // 
            // btnRegresar
            // 
            this.btnRegresar.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRegresar.Location = new System.Drawing.Point(33, 506);
            this.btnRegresar.Name = "btnRegresar";
            this.btnRegresar.Size = new System.Drawing.Size(106, 32);
            this.btnRegresar.TabIndex = 0;
            this.btnRegresar.Text = "Regresar";
            this.btnRegresar.UseVisualStyleBackColor = true;
            this.btnRegresar.Click += new System.EventHandler(this.btnRegresar_Click);
            // 
            // btnCitas
            // 
            this.btnCitas.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCitas.Location = new System.Drawing.Point(666, 506);
            this.btnCitas.Name = "btnCitas";
            this.btnCitas.Size = new System.Drawing.Size(88, 32);
            this.btnCitas.TabIndex = 1;
            this.btnCitas.Text = "Citas";
            this.btnCitas.UseVisualStyleBackColor = true;
            this.btnCitas.Click += new System.EventHandler(this.btnCitas_Click);
            // 
            // gBNiños
            // 
            this.gBNiños.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.gBNiños.Controls.Add(this.pBNiños);
            this.gBNiños.Controls.Add(this.lblPrecioN);
            this.gBNiños.Controls.Add(this.ckBNiños);
            this.gBNiños.Location = new System.Drawing.Point(113, 74);
            this.gBNiños.Name = "gBNiños";
            this.gBNiños.Size = new System.Drawing.Size(237, 330);
            this.gBNiños.TabIndex = 3;
            this.gBNiños.TabStop = false;
            // 
            // pBNiños
            // 
            this.pBNiños.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pBNiños.BackgroundImage")));
            this.pBNiños.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pBNiños.Location = new System.Drawing.Point(37, 48);
            this.pBNiños.Name = "pBNiños";
            this.pBNiños.Size = new System.Drawing.Size(160, 110);
            this.pBNiños.TabIndex = 2;
            this.pBNiños.TabStop = false;
            // 
            // lblPrecioN
            // 
            this.lblPrecioN.AutoSize = true;
            this.lblPrecioN.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrecioN.Location = new System.Drawing.Point(56, 254);
            this.lblPrecioN.Name = "lblPrecioN";
            this.lblPrecioN.Size = new System.Drawing.Size(103, 21);
            this.lblPrecioN.TabIndex = 1;
            this.lblPrecioN.Text = "Precio: $240";
            // 
            // ckBNiños
            // 
            this.ckBNiños.AutoSize = true;
            this.ckBNiños.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckBNiños.Location = new System.Drawing.Point(60, 209);
            this.ckBNiños.Name = "ckBNiños";
            this.ckBNiños.Size = new System.Drawing.Size(94, 28);
            this.ckBNiños.TabIndex = 0;
            this.ckBNiños.Text = "NIÑOS";
            this.ckBNiños.UseVisualStyleBackColor = true;
            // 
            // gBAdulto
            // 
            this.gBAdulto.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.gBAdulto.Controls.Add(this.pBAdultos);
            this.gBAdulto.Controls.Add(this.lblPrecioA);
            this.gBAdulto.Controls.Add(this.ckBAdultos);
            this.gBAdulto.Location = new System.Drawing.Point(444, 74);
            this.gBAdulto.Name = "gBAdulto";
            this.gBAdulto.Size = new System.Drawing.Size(235, 330);
            this.gBAdulto.TabIndex = 4;
            this.gBAdulto.TabStop = false;
            // 
            // pBAdultos
            // 
            this.pBAdultos.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pBAdultos.BackgroundImage")));
            this.pBAdultos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pBAdultos.Location = new System.Drawing.Point(42, 48);
            this.pBAdultos.Name = "pBAdultos";
            this.pBAdultos.Size = new System.Drawing.Size(145, 110);
            this.pBAdultos.TabIndex = 3;
            this.pBAdultos.TabStop = false;
            // 
            // lblPrecioA
            // 
            this.lblPrecioA.AutoSize = true;
            this.lblPrecioA.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrecioA.Location = new System.Drawing.Point(49, 254);
            this.lblPrecioA.Name = "lblPrecioA";
            this.lblPrecioA.Size = new System.Drawing.Size(103, 21);
            this.lblPrecioA.TabIndex = 1;
            this.lblPrecioA.Text = "Precio: $400";
            // 
            // ckBAdultos
            // 
            this.ckBAdultos.AutoSize = true;
            this.ckBAdultos.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckBAdultos.Location = new System.Drawing.Point(53, 209);
            this.ckBAdultos.Name = "ckBAdultos";
            this.ckBAdultos.Size = new System.Drawing.Size(127, 28);
            this.ckBAdultos.TabIndex = 0;
            this.ckBAdultos.Text = "ADULTOS";
            this.ckBAdultos.UseVisualStyleBackColor = true;
            // 
            // Extracciones
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(792, 550);
            this.Controls.Add(this.gBAdulto);
            this.Controls.Add(this.gBNiños);
            this.Controls.Add(this.btnCitas);
            this.Controls.Add(this.btnRegresar);
            this.Name = "Extracciones";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Extracciones";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Extracciones_FormClosed);
            this.gBNiños.ResumeLayout(false);
            this.gBNiños.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pBNiños)).EndInit();
            this.gBAdulto.ResumeLayout(false);
            this.gBAdulto.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pBAdultos)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnRegresar;
        private System.Windows.Forms.Button btnCitas;
        private System.Windows.Forms.GroupBox gBNiños;
        private System.Windows.Forms.Label lblPrecioN;
        private System.Windows.Forms.CheckBox ckBNiños;
        private System.Windows.Forms.GroupBox gBAdulto;
        private System.Windows.Forms.Label lblPrecioA;
        private System.Windows.Forms.CheckBox ckBAdultos;
        private System.Windows.Forms.PictureBox pBNiños;
        private System.Windows.Forms.PictureBox pBAdultos;
    }
}