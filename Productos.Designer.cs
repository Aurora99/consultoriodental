﻿namespace ConsultorioDental
{
    partial class Productos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Productos));
            this.gBCepillitos = new System.Windows.Forms.GroupBox();
            this.lblPrecioCepillitos = new System.Windows.Forms.Label();
            this.ckBCepillitos = new System.Windows.Forms.CheckBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.gBCepillo = new System.Windows.Forms.GroupBox();
            this.lblPrecioCepillo = new System.Windows.Forms.Label();
            this.ckBCepillo = new System.Windows.Forms.CheckBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.gBCera = new System.Windows.Forms.GroupBox();
            this.lblPrecioCera = new System.Windows.Forms.Label();
            this.ckBCera = new System.Windows.Forms.CheckBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.gBEnguaje = new System.Windows.Forms.GroupBox();
            this.lblPrecioEnguaje = new System.Windows.Forms.Label();
            this.ckBEnguaje = new System.Windows.Forms.CheckBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.gBHilo = new System.Windows.Forms.GroupBox();
            this.lblPrecioHilo = new System.Windows.Forms.Label();
            this.ckBHilo = new System.Windows.Forms.CheckBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.gBPasta = new System.Windows.Forms.GroupBox();
            this.lblPrecioPasta = new System.Windows.Forms.Label();
            this.ckBPasta = new System.Windows.Forms.CheckBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.btnRegresar = new System.Windows.Forms.Button();
            this.btnCitas = new System.Windows.Forms.Button();
            this.gBCepillitos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.gBCepillo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.gBCera.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.gBEnguaje.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.gBHilo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.gBPasta.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSalir
            // 
            this.btnSalir.Location = new System.Drawing.Point(703, 613);
            this.btnSalir.Size = new System.Drawing.Size(104, 33);
            // 
            // gBCepillitos
            // 
            this.gBCepillitos.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.gBCepillitos.Controls.Add(this.lblPrecioCepillitos);
            this.gBCepillitos.Controls.Add(this.ckBCepillitos);
            this.gBCepillitos.Controls.Add(this.pictureBox1);
            this.gBCepillitos.Location = new System.Drawing.Point(56, 54);
            this.gBCepillitos.Name = "gBCepillitos";
            this.gBCepillitos.Size = new System.Drawing.Size(212, 248);
            this.gBCepillitos.TabIndex = 1;
            this.gBCepillitos.TabStop = false;
            // 
            // lblPrecioCepillitos
            // 
            this.lblPrecioCepillitos.AutoSize = true;
            this.lblPrecioCepillitos.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrecioCepillitos.Location = new System.Drawing.Point(79, 195);
            this.lblPrecioCepillitos.Name = "lblPrecioCepillitos";
            this.lblPrecioCepillitos.Size = new System.Drawing.Size(33, 19);
            this.lblPrecioCepillitos.TabIndex = 2;
            this.lblPrecioCepillitos.Text = "$70";
            // 
            // ckBCepillitos
            // 
            this.ckBCepillitos.AutoSize = true;
            this.ckBCepillitos.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckBCepillitos.Location = new System.Drawing.Point(40, 154);
            this.ckBCepillitos.Name = "ckBCepillitos";
            this.ckBCepillitos.Size = new System.Drawing.Size(145, 26);
            this.ckBCepillitos.TabIndex = 1;
            this.ckBCepillitos.Text = "CEPILLITOS";
            this.ckBCepillitos.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(40, 31);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(111, 98);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // gBCepillo
            // 
            this.gBCepillo.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.gBCepillo.Controls.Add(this.lblPrecioCepillo);
            this.gBCepillo.Controls.Add(this.ckBCepillo);
            this.gBCepillo.Controls.Add(this.pictureBox2);
            this.gBCepillo.Location = new System.Drawing.Point(330, 54);
            this.gBCepillo.Name = "gBCepillo";
            this.gBCepillo.Size = new System.Drawing.Size(212, 248);
            this.gBCepillo.TabIndex = 3;
            this.gBCepillo.TabStop = false;
            // 
            // lblPrecioCepillo
            // 
            this.lblPrecioCepillo.AutoSize = true;
            this.lblPrecioCepillo.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrecioCepillo.Location = new System.Drawing.Point(76, 195);
            this.lblPrecioCepillo.Name = "lblPrecioCepillo";
            this.lblPrecioCepillo.Size = new System.Drawing.Size(33, 19);
            this.lblPrecioCepillo.TabIndex = 2;
            this.lblPrecioCepillo.Text = "$90";
            // 
            // ckBCepillo
            // 
            this.ckBCepillo.AutoSize = true;
            this.ckBCepillo.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckBCepillo.Location = new System.Drawing.Point(40, 154);
            this.ckBCepillo.Name = "ckBCepillo";
            this.ckBCepillo.Size = new System.Drawing.Size(125, 26);
            this.ckBCepillo.TabIndex = 1;
            this.ckBCepillo.Text = "CEPILLOS";
            this.ckBCepillo.UseVisualStyleBackColor = true;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox2.BackgroundImage")));
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Location = new System.Drawing.Point(40, 31);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(111, 98);
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            // 
            // gBCera
            // 
            this.gBCera.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.gBCera.Controls.Add(this.lblPrecioCera);
            this.gBCera.Controls.Add(this.ckBCera);
            this.gBCera.Controls.Add(this.pictureBox3);
            this.gBCera.Location = new System.Drawing.Point(603, 54);
            this.gBCera.Name = "gBCera";
            this.gBCera.Size = new System.Drawing.Size(212, 248);
            this.gBCera.TabIndex = 3;
            this.gBCera.TabStop = false;
            // 
            // lblPrecioCera
            // 
            this.lblPrecioCera.AutoSize = true;
            this.lblPrecioCera.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrecioCera.Location = new System.Drawing.Point(85, 183);
            this.lblPrecioCera.Name = "lblPrecioCera";
            this.lblPrecioCera.Size = new System.Drawing.Size(33, 19);
            this.lblPrecioCera.TabIndex = 2;
            this.lblPrecioCera.Text = "$55";
            // 
            // ckBCera
            // 
            this.ckBCera.AutoSize = true;
            this.ckBCera.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckBCera.Location = new System.Drawing.Point(56, 154);
            this.ckBCera.Name = "ckBCera";
            this.ckBCera.Size = new System.Drawing.Size(83, 26);
            this.ckBCera.TabIndex = 1;
            this.ckBCera.Text = "CERA";
            this.ckBCera.UseVisualStyleBackColor = true;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox3.BackgroundImage")));
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox3.Location = new System.Drawing.Point(56, 31);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(111, 98);
            this.pictureBox3.TabIndex = 0;
            this.pictureBox3.TabStop = false;
            // 
            // gBEnguaje
            // 
            this.gBEnguaje.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.gBEnguaje.Controls.Add(this.lblPrecioEnguaje);
            this.gBEnguaje.Controls.Add(this.ckBEnguaje);
            this.gBEnguaje.Controls.Add(this.pictureBox4);
            this.gBEnguaje.Location = new System.Drawing.Point(56, 329);
            this.gBEnguaje.Name = "gBEnguaje";
            this.gBEnguaje.Size = new System.Drawing.Size(212, 248);
            this.gBEnguaje.TabIndex = 3;
            this.gBEnguaje.TabStop = false;
            // 
            // lblPrecioEnguaje
            // 
            this.lblPrecioEnguaje.AutoSize = true;
            this.lblPrecioEnguaje.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrecioEnguaje.Location = new System.Drawing.Point(79, 195);
            this.lblPrecioEnguaje.Name = "lblPrecioEnguaje";
            this.lblPrecioEnguaje.Size = new System.Drawing.Size(33, 19);
            this.lblPrecioEnguaje.TabIndex = 2;
            this.lblPrecioEnguaje.Text = "$80";
            // 
            // ckBEnguaje
            // 
            this.ckBEnguaje.AutoSize = true;
            this.ckBEnguaje.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckBEnguaje.Location = new System.Drawing.Point(40, 154);
            this.ckBEnguaje.Name = "ckBEnguaje";
            this.ckBEnguaje.Size = new System.Drawing.Size(120, 26);
            this.ckBEnguaje.TabIndex = 1;
            this.ckBEnguaje.Text = "ENGUAJE";
            this.ckBEnguaje.UseVisualStyleBackColor = true;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox4.BackgroundImage")));
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox4.Location = new System.Drawing.Point(40, 31);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(111, 98);
            this.pictureBox4.TabIndex = 0;
            this.pictureBox4.TabStop = false;
            // 
            // gBHilo
            // 
            this.gBHilo.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.gBHilo.Controls.Add(this.lblPrecioHilo);
            this.gBHilo.Controls.Add(this.ckBHilo);
            this.gBHilo.Controls.Add(this.pictureBox5);
            this.gBHilo.Location = new System.Drawing.Point(339, 329);
            this.gBHilo.Name = "gBHilo";
            this.gBHilo.Size = new System.Drawing.Size(212, 248);
            this.gBHilo.TabIndex = 3;
            this.gBHilo.TabStop = false;
            // 
            // lblPrecioHilo
            // 
            this.lblPrecioHilo.AutoSize = true;
            this.lblPrecioHilo.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrecioHilo.Location = new System.Drawing.Point(67, 195);
            this.lblPrecioHilo.Name = "lblPrecioHilo";
            this.lblPrecioHilo.Size = new System.Drawing.Size(33, 19);
            this.lblPrecioHilo.TabIndex = 2;
            this.lblPrecioHilo.Text = "$60";
            // 
            // ckBHilo
            // 
            this.ckBHilo.AutoSize = true;
            this.ckBHilo.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckBHilo.Location = new System.Drawing.Point(20, 154);
            this.ckBHilo.Name = "ckBHilo";
            this.ckBHilo.Size = new System.Drawing.Size(160, 26);
            this.ckBHilo.TabIndex = 1;
            this.ckBHilo.Text = "HILO DENTAL";
            this.ckBHilo.UseVisualStyleBackColor = true;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox5.BackgroundImage")));
            this.pictureBox5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox5.Location = new System.Drawing.Point(40, 31);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(128, 98);
            this.pictureBox5.TabIndex = 0;
            this.pictureBox5.TabStop = false;
            // 
            // gBPasta
            // 
            this.gBPasta.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.gBPasta.Controls.Add(this.lblPrecioPasta);
            this.gBPasta.Controls.Add(this.ckBPasta);
            this.gBPasta.Controls.Add(this.pictureBox6);
            this.gBPasta.Location = new System.Drawing.Point(603, 329);
            this.gBPasta.Name = "gBPasta";
            this.gBPasta.Size = new System.Drawing.Size(212, 248);
            this.gBPasta.TabIndex = 4;
            this.gBPasta.TabStop = false;
            // 
            // lblPrecioPasta
            // 
            this.lblPrecioPasta.AutoSize = true;
            this.lblPrecioPasta.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrecioPasta.Location = new System.Drawing.Point(85, 195);
            this.lblPrecioPasta.Name = "lblPrecioPasta";
            this.lblPrecioPasta.Size = new System.Drawing.Size(33, 19);
            this.lblPrecioPasta.TabIndex = 2;
            this.lblPrecioPasta.Text = "$75";
            // 
            // ckBPasta
            // 
            this.ckBPasta.AutoSize = true;
            this.ckBPasta.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckBPasta.Location = new System.Drawing.Point(35, 154);
            this.ckBPasta.Name = "ckBPasta";
            this.ckBPasta.Size = new System.Drawing.Size(171, 26);
            this.ckBPasta.TabIndex = 1;
            this.ckBPasta.Text = "PASTA DENTAL";
            this.ckBPasta.UseVisualStyleBackColor = true;
            this.ckBPasta.CheckedChanged += new System.EventHandler(this.ckBPasta_CheckedChanged);
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox6.BackgroundImage")));
            this.pictureBox6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox6.Location = new System.Drawing.Point(40, 31);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(111, 98);
            this.pictureBox6.TabIndex = 0;
            this.pictureBox6.TabStop = false;
            // 
            // btnRegresar
            // 
            this.btnRegresar.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRegresar.Location = new System.Drawing.Point(56, 621);
            this.btnRegresar.Name = "btnRegresar";
            this.btnRegresar.Size = new System.Drawing.Size(91, 29);
            this.btnRegresar.TabIndex = 5;
            this.btnRegresar.Text = "Regresar";
            this.btnRegresar.UseVisualStyleBackColor = true;
            this.btnRegresar.Click += new System.EventHandler(this.btnRegresar_Click);
            // 
            // btnCitas
            // 
            this.btnCitas.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCitas.Location = new System.Drawing.Point(390, 617);
            this.btnCitas.Name = "btnCitas";
            this.btnCitas.Size = new System.Drawing.Size(91, 29);
            this.btnCitas.TabIndex = 6;
            this.btnCitas.Text = "Citas";
            this.btnCitas.UseVisualStyleBackColor = true;
            this.btnCitas.Click += new System.EventHandler(this.btnCitas_Click);
            // 
            // Productos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(850, 666);
            this.Controls.Add(this.btnCitas);
            this.Controls.Add(this.btnRegresar);
            this.Controls.Add(this.gBPasta);
            this.Controls.Add(this.gBHilo);
            this.Controls.Add(this.gBEnguaje);
            this.Controls.Add(this.gBCera);
            this.Controls.Add(this.gBCepillo);
            this.Controls.Add(this.gBCepillitos);
            this.Name = "Productos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Productos";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Productos_FormClosed);
            this.Load += new System.EventHandler(this.Productos_Load);
            this.Controls.SetChildIndex(this.btnSalir, 0);
            this.Controls.SetChildIndex(this.gBCepillitos, 0);
            this.Controls.SetChildIndex(this.gBCepillo, 0);
            this.Controls.SetChildIndex(this.gBCera, 0);
            this.Controls.SetChildIndex(this.gBEnguaje, 0);
            this.Controls.SetChildIndex(this.gBHilo, 0);
            this.Controls.SetChildIndex(this.gBPasta, 0);
            this.Controls.SetChildIndex(this.btnRegresar, 0);
            this.Controls.SetChildIndex(this.btnCitas, 0);
            this.gBCepillitos.ResumeLayout(false);
            this.gBCepillitos.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.gBCepillo.ResumeLayout(false);
            this.gBCepillo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.gBCera.ResumeLayout(false);
            this.gBCera.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.gBEnguaje.ResumeLayout(false);
            this.gBEnguaje.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.gBHilo.ResumeLayout(false);
            this.gBHilo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.gBPasta.ResumeLayout(false);
            this.gBPasta.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gBCepillitos;
        private System.Windows.Forms.Label lblPrecioCepillitos;
        private System.Windows.Forms.CheckBox ckBCepillitos;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox gBCepillo;
        private System.Windows.Forms.Label lblPrecioCepillo;
        private System.Windows.Forms.CheckBox ckBCepillo;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.GroupBox gBCera;
        private System.Windows.Forms.Label lblPrecioCera;
        private System.Windows.Forms.CheckBox ckBCera;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.GroupBox gBEnguaje;
        private System.Windows.Forms.Label lblPrecioEnguaje;
        private System.Windows.Forms.CheckBox ckBEnguaje;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.GroupBox gBHilo;
        private System.Windows.Forms.Label lblPrecioHilo;
        private System.Windows.Forms.CheckBox ckBHilo;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.GroupBox gBPasta;
        private System.Windows.Forms.Label lblPrecioPasta;
        private System.Windows.Forms.CheckBox ckBPasta;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Button btnRegresar;
        private System.Windows.Forms.Button btnCitas;
    }
}