﻿namespace ConsultorioDental
{
    partial class Ortodoncía
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Ortodoncía));
            this.gBBrackets = new System.Windows.Forms.GroupBox();
            this.lblBrackets2 = new System.Windows.Forms.Label();
            this.lblBrackets1 = new System.Windows.Forms.Label();
            this.ckBBrackets = new System.Windows.Forms.CheckBox();
            this.pBBrackets = new System.Windows.Forms.PictureBox();
            this.btnRegresar = new System.Windows.Forms.Button();
            this.btnCitas = new System.Windows.Forms.Button();
            this.gBRetenedores = new System.Windows.Forms.GroupBox();
            this.lblRetenedores = new System.Windows.Forms.Label();
            this.ckBRetenedores = new System.Windows.Forms.CheckBox();
            this.pBRetenedores = new System.Windows.Forms.PictureBox();
            this.gBAparato = new System.Windows.Forms.GroupBox();
            this.lblAparato = new System.Windows.Forms.Label();
            this.ckBAparato = new System.Windows.Forms.CheckBox();
            this.pBAparato = new System.Windows.Forms.PictureBox();
            this.gBPaladar = new System.Windows.Forms.GroupBox();
            this.lblPaladar2 = new System.Windows.Forms.Label();
            this.lblPaladar1 = new System.Windows.Forms.Label();
            this.ckBPaladar = new System.Windows.Forms.CheckBox();
            this.pBPaladar = new System.Windows.Forms.PictureBox();
            this.gBBrackets.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pBBrackets)).BeginInit();
            this.gBRetenedores.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pBRetenedores)).BeginInit();
            this.gBAparato.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pBAparato)).BeginInit();
            this.gBPaladar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pBPaladar)).BeginInit();
            this.SuspendLayout();
            // 
            // gBBrackets
            // 
            this.gBBrackets.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.gBBrackets.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.gBBrackets.Controls.Add(this.lblBrackets2);
            this.gBBrackets.Controls.Add(this.lblBrackets1);
            this.gBBrackets.Controls.Add(this.ckBBrackets);
            this.gBBrackets.Controls.Add(this.pBBrackets);
            this.gBBrackets.Location = new System.Drawing.Point(127, 54);
            this.gBBrackets.Name = "gBBrackets";
            this.gBBrackets.Size = new System.Drawing.Size(228, 256);
            this.gBBrackets.TabIndex = 0;
            this.gBBrackets.TabStop = false;
            // 
            // lblBrackets2
            // 
            this.lblBrackets2.Location = new System.Drawing.Point(21, 209);
            this.lblBrackets2.Name = "lblBrackets2";
            this.lblBrackets2.Size = new System.Drawing.Size(168, 32);
            this.lblBrackets2.TabIndex = 3;
            this.lblBrackets2.Text = "Costo por cita:\r\n$450\r\n";
            // 
            // lblBrackets1
            // 
            this.lblBrackets1.Location = new System.Drawing.Point(21, 186);
            this.lblBrackets1.Name = "lblBrackets1";
            this.lblBrackets1.Size = new System.Drawing.Size(171, 23);
            this.lblBrackets1.TabIndex = 2;
            this.lblBrackets1.Text = "Colocación: $4000";
            // 
            // ckBBrackets
            // 
            this.ckBBrackets.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckBBrackets.Location = new System.Drawing.Point(55, 144);
            this.ckBBrackets.Name = "ckBBrackets";
            this.ckBBrackets.Size = new System.Drawing.Size(104, 24);
            this.ckBBrackets.TabIndex = 1;
            this.ckBBrackets.Text = "BRACKETS";
            this.ckBBrackets.UseVisualStyleBackColor = true;
            // 
            // pBBrackets
            // 
            this.pBBrackets.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pBBrackets.BackgroundImage")));
            this.pBBrackets.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pBBrackets.Location = new System.Drawing.Point(45, 19);
            this.pBBrackets.Name = "pBBrackets";
            this.pBBrackets.Size = new System.Drawing.Size(146, 97);
            this.pBBrackets.TabIndex = 0;
            this.pBBrackets.TabStop = false;
            // 
            // btnRegresar
            // 
            this.btnRegresar.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRegresar.Location = new System.Drawing.Point(37, 632);
            this.btnRegresar.Name = "btnRegresar";
            this.btnRegresar.Size = new System.Drawing.Size(91, 29);
            this.btnRegresar.TabIndex = 4;
            this.btnRegresar.Text = "Regresar";
            this.btnRegresar.UseVisualStyleBackColor = true;
            this.btnRegresar.Click += new System.EventHandler(this.btnRegresar_Click);
            // 
            // btnCitas
            // 
            this.btnCitas.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCitas.Location = new System.Drawing.Point(689, 632);
            this.btnCitas.Name = "btnCitas";
            this.btnCitas.Size = new System.Drawing.Size(91, 29);
            this.btnCitas.TabIndex = 5;
            this.btnCitas.Text = "Citas";
            this.btnCitas.UseVisualStyleBackColor = true;
            this.btnCitas.Click += new System.EventHandler(this.btnCitas_Click);
            // 
            // gBRetenedores
            // 
            this.gBRetenedores.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.gBRetenedores.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.gBRetenedores.Controls.Add(this.lblRetenedores);
            this.gBRetenedores.Controls.Add(this.ckBRetenedores);
            this.gBRetenedores.Controls.Add(this.pBRetenedores);
            this.gBRetenedores.Location = new System.Drawing.Point(463, 54);
            this.gBRetenedores.Name = "gBRetenedores";
            this.gBRetenedores.Size = new System.Drawing.Size(228, 256);
            this.gBRetenedores.TabIndex = 4;
            this.gBRetenedores.TabStop = false;
            // 
            // lblRetenedores
            // 
            this.lblRetenedores.Location = new System.Drawing.Point(21, 186);
            this.lblRetenedores.Name = "lblRetenedores";
            this.lblRetenedores.Size = new System.Drawing.Size(171, 23);
            this.lblRetenedores.TabIndex = 2;
            this.lblRetenedores.Text = "Colocación: $400";
            // 
            // ckBRetenedores
            // 
            this.ckBRetenedores.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckBRetenedores.Location = new System.Drawing.Point(55, 144);
            this.ckBRetenedores.Name = "ckBRetenedores";
            this.ckBRetenedores.Size = new System.Drawing.Size(118, 24);
            this.ckBRetenedores.TabIndex = 1;
            this.ckBRetenedores.Text = "RETENEDORES";
            this.ckBRetenedores.UseVisualStyleBackColor = true;
            // 
            // pBRetenedores
            // 
            this.pBRetenedores.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pBRetenedores.BackgroundImage")));
            this.pBRetenedores.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pBRetenedores.Location = new System.Drawing.Point(43, 19);
            this.pBRetenedores.Name = "pBRetenedores";
            this.pBRetenedores.Size = new System.Drawing.Size(149, 97);
            this.pBRetenedores.TabIndex = 0;
            this.pBRetenedores.TabStop = false;
            // 
            // gBAparato
            // 
            this.gBAparato.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.gBAparato.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.gBAparato.Controls.Add(this.lblAparato);
            this.gBAparato.Controls.Add(this.ckBAparato);
            this.gBAparato.Controls.Add(this.pBAparato);
            this.gBAparato.Location = new System.Drawing.Point(127, 349);
            this.gBAparato.Name = "gBAparato";
            this.gBAparato.Size = new System.Drawing.Size(228, 256);
            this.gBAparato.TabIndex = 4;
            this.gBAparato.TabStop = false;
            // 
            // lblAparato
            // 
            this.lblAparato.Location = new System.Drawing.Point(21, 186);
            this.lblAparato.Name = "lblAparato";
            this.lblAparato.Size = new System.Drawing.Size(171, 23);
            this.lblAparato.TabIndex = 2;
            this.lblAparato.Text = "Costo: $200";
            // 
            // ckBAparato
            // 
            this.ckBAparato.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckBAparato.Location = new System.Drawing.Point(45, 144);
            this.ckBAparato.Name = "ckBAparato";
            this.ckBAparato.Size = new System.Drawing.Size(147, 24);
            this.ckBAparato.TabIndex = 1;
            this.ckBAparato.Text = "APARATO \"BRACKET\"";
            this.ckBAparato.UseVisualStyleBackColor = true;
            // 
            // pBAparato
            // 
            this.pBAparato.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pBAparato.BackgroundImage")));
            this.pBAparato.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pBAparato.Location = new System.Drawing.Point(45, 19);
            this.pBAparato.Name = "pBAparato";
            this.pBAparato.Size = new System.Drawing.Size(146, 105);
            this.pBAparato.TabIndex = 0;
            this.pBAparato.TabStop = false;
            // 
            // gBPaladar
            // 
            this.gBPaladar.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.gBPaladar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.gBPaladar.Controls.Add(this.lblPaladar2);
            this.gBPaladar.Controls.Add(this.lblPaladar1);
            this.gBPaladar.Controls.Add(this.ckBPaladar);
            this.gBPaladar.Controls.Add(this.pBPaladar);
            this.gBPaladar.Location = new System.Drawing.Point(463, 349);
            this.gBPaladar.Name = "gBPaladar";
            this.gBPaladar.Size = new System.Drawing.Size(228, 256);
            this.gBPaladar.TabIndex = 4;
            this.gBPaladar.TabStop = false;
            // 
            // lblPaladar2
            // 
            this.lblPaladar2.Location = new System.Drawing.Point(24, 209);
            this.lblPaladar2.Name = "lblPaladar2";
            this.lblPaladar2.Size = new System.Drawing.Size(168, 32);
            this.lblPaladar2.TabIndex = 3;
            this.lblPaladar2.Text = "Costo por cita:\r\n$400";
            // 
            // lblPaladar1
            // 
            this.lblPaladar1.Location = new System.Drawing.Point(21, 186);
            this.lblPaladar1.Name = "lblPaladar1";
            this.lblPaladar1.Size = new System.Drawing.Size(171, 23);
            this.lblPaladar1.TabIndex = 2;
            this.lblPaladar1.Text = "Colocacion: $3500";
            // 
            // ckBPaladar
            // 
            this.ckBPaladar.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckBPaladar.Location = new System.Drawing.Point(55, 144);
            this.ckBPaladar.Name = "ckBPaladar";
            this.ckBPaladar.Size = new System.Drawing.Size(104, 24);
            this.ckBPaladar.TabIndex = 1;
            this.ckBPaladar.Text = "PALADAR";
            this.ckBPaladar.UseVisualStyleBackColor = true;
            // 
            // pBPaladar
            // 
            this.pBPaladar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pBPaladar.BackgroundImage")));
            this.pBPaladar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pBPaladar.Location = new System.Drawing.Point(43, 19);
            this.pBPaladar.Name = "pBPaladar";
            this.pBPaladar.Size = new System.Drawing.Size(149, 97);
            this.pBPaladar.TabIndex = 0;
            this.pBPaladar.TabStop = false;
            // 
            // Ortodoncía
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(810, 688);
            this.Controls.Add(this.gBPaladar);
            this.Controls.Add(this.gBAparato);
            this.Controls.Add(this.gBRetenedores);
            this.Controls.Add(this.btnCitas);
            this.Controls.Add(this.btnRegresar);
            this.Controls.Add(this.gBBrackets);
            this.Name = "Ortodoncía";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ortodoncía";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Ortodoncía_FormClosed);
            this.Load += new System.EventHandler(this.Ortodoncía_Load);
            this.gBBrackets.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pBBrackets)).EndInit();
            this.gBRetenedores.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pBRetenedores)).EndInit();
            this.gBAparato.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pBAparato)).EndInit();
            this.gBPaladar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pBPaladar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gBBrackets;
        private System.Windows.Forms.Button btnRegresar;
        private System.Windows.Forms.Button btnCitas;
        public System.Windows.Forms.Label lblBrackets2;
        public System.Windows.Forms.Label lblBrackets1;
        public System.Windows.Forms.CheckBox ckBBrackets;
        private System.Windows.Forms.PictureBox pBBrackets;
        private System.Windows.Forms.GroupBox gBRetenedores;
        public System.Windows.Forms.Label lblRetenedores;
        public System.Windows.Forms.CheckBox ckBRetenedores;
        private System.Windows.Forms.PictureBox pBRetenedores;
        private System.Windows.Forms.GroupBox gBAparato;
        public System.Windows.Forms.Label lblAparato;
        public System.Windows.Forms.CheckBox ckBAparato;
        private System.Windows.Forms.PictureBox pBAparato;
        private System.Windows.Forms.GroupBox gBPaladar;
        public System.Windows.Forms.Label lblPaladar2;
        public System.Windows.Forms.Label lblPaladar1;
        public System.Windows.Forms.CheckBox ckBPaladar;
        private System.Windows.Forms.PictureBox pBPaladar;
    }
}