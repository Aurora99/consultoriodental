﻿namespace ConsultorioDental
{
    partial class Ventana_Cliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Ventana_Cliente));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnCitas = new System.Windows.Forms.Button();
            this.btnPaginaP = new System.Windows.Forms.Button();
            this.lblTCodigo = new System.Windows.Forms.Label();
            this.lblCodigo = new System.Windows.Forms.Label();
            this.lblTUsuario = new System.Windows.Forms.Label();
            this.lblUsuario = new System.Windows.Forms.Label();
            this.lblTCliente = new System.Windows.Forms.Label();
            this.lblCliente = new System.Windows.Forms.Label();
            this.btnTutorial = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSalir
            // 
            this.btnSalir.Location = new System.Drawing.Point(446, 390);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(146, 33);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(252, 163);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // btnCitas
            // 
            this.btnCitas.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCitas.Location = new System.Drawing.Point(446, 334);
            this.btnCitas.Name = "btnCitas";
            this.btnCitas.Size = new System.Drawing.Size(77, 32);
            this.btnCitas.TabIndex = 17;
            this.btnCitas.Text = "Citas";
            this.btnCitas.UseVisualStyleBackColor = true;
            this.btnCitas.Click += new System.EventHandler(this.btnCitas_Click);
            // 
            // btnPaginaP
            // 
            this.btnPaginaP.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPaginaP.Location = new System.Drawing.Point(423, 216);
            this.btnPaginaP.Name = "btnPaginaP";
            this.btnPaginaP.Size = new System.Drawing.Size(130, 36);
            this.btnPaginaP.TabIndex = 16;
            this.btnPaginaP.Text = "Pagina Principal";
            this.btnPaginaP.UseVisualStyleBackColor = true;
            this.btnPaginaP.Click += new System.EventHandler(this.btnPaginaP_Click);
            // 
            // lblTCodigo
            // 
            this.lblTCodigo.BackColor = System.Drawing.SystemColors.ControlDark;
            this.lblTCodigo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTCodigo.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTCodigo.Location = new System.Drawing.Point(146, 375);
            this.lblTCodigo.Name = "lblTCodigo";
            this.lblTCodigo.Size = new System.Drawing.Size(233, 23);
            this.lblTCodigo.TabIndex = 15;
            // 
            // lblCodigo
            // 
            this.lblCodigo.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodigo.Location = new System.Drawing.Point(34, 375);
            this.lblCodigo.Name = "lblCodigo";
            this.lblCodigo.Size = new System.Drawing.Size(62, 23);
            this.lblCodigo.TabIndex = 14;
            this.lblCodigo.Text = "Codigo:";
            // 
            // lblTUsuario
            // 
            this.lblTUsuario.BackColor = System.Drawing.SystemColors.ControlDark;
            this.lblTUsuario.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTUsuario.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTUsuario.Location = new System.Drawing.Point(146, 318);
            this.lblTUsuario.Name = "lblTUsuario";
            this.lblTUsuario.Size = new System.Drawing.Size(233, 22);
            this.lblTUsuario.TabIndex = 13;
            // 
            // lblUsuario
            // 
            this.lblUsuario.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsuario.Location = new System.Drawing.Point(34, 318);
            this.lblUsuario.Name = "lblUsuario";
            this.lblUsuario.Size = new System.Drawing.Size(70, 22);
            this.lblUsuario.TabIndex = 12;
            this.lblUsuario.Text = "Usuario:";
            // 
            // lblTCliente
            // 
            this.lblTCliente.BackColor = System.Drawing.SystemColors.ControlDark;
            this.lblTCliente.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTCliente.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTCliente.Location = new System.Drawing.Point(146, 247);
            this.lblTCliente.Name = "lblTCliente";
            this.lblTCliente.Size = new System.Drawing.Size(233, 22);
            this.lblTCliente.TabIndex = 11;
            // 
            // lblCliente
            // 
            this.lblCliente.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.Location = new System.Drawing.Point(34, 247);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.Size = new System.Drawing.Size(70, 22);
            this.lblCliente.TabIndex = 10;
            this.lblCliente.Text = "Cliente:";
            // 
            // btnTutorial
            // 
            this.btnTutorial.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTutorial.Location = new System.Drawing.Point(434, 271);
            this.btnTutorial.Name = "btnTutorial";
            this.btnTutorial.Size = new System.Drawing.Size(106, 36);
            this.btnTutorial.TabIndex = 18;
            this.btnTutorial.Text = "TUTORIAL";
            this.btnTutorial.UseVisualStyleBackColor = true;
            this.btnTutorial.Click += new System.EventHandler(this.btnTutorial_Click);
            // 
            // Ventana_Cliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(579, 498);
            this.Controls.Add(this.btnTutorial);
            this.Controls.Add(this.btnCitas);
            this.Controls.Add(this.btnPaginaP);
            this.Controls.Add(this.lblTCodigo);
            this.Controls.Add(this.lblCodigo);
            this.Controls.Add(this.lblTUsuario);
            this.Controls.Add(this.lblUsuario);
            this.Controls.Add(this.lblTCliente);
            this.Controls.Add(this.lblCliente);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Ventana_Cliente";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ventana_Cliente";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Ventana_Cliente_FormClosed);
            this.Load += new System.EventHandler(this.Ventana_Cliente_Load);
            this.Controls.SetChildIndex(this.btnSalir, 0);
            this.Controls.SetChildIndex(this.pictureBox1, 0);
            this.Controls.SetChildIndex(this.lblCliente, 0);
            this.Controls.SetChildIndex(this.lblTCliente, 0);
            this.Controls.SetChildIndex(this.lblUsuario, 0);
            this.Controls.SetChildIndex(this.lblTUsuario, 0);
            this.Controls.SetChildIndex(this.lblCodigo, 0);
            this.Controls.SetChildIndex(this.lblTCodigo, 0);
            this.Controls.SetChildIndex(this.btnPaginaP, 0);
            this.Controls.SetChildIndex(this.btnCitas, 0);
            this.Controls.SetChildIndex(this.btnTutorial, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnCitas;
        private System.Windows.Forms.Button btnPaginaP;
        private System.Windows.Forms.Label lblTCodigo;
        private System.Windows.Forms.Label lblCodigo;
        private System.Windows.Forms.Label lblTUsuario;
        private System.Windows.Forms.Label lblUsuario;
        private System.Windows.Forms.Label lblTCliente;
        private System.Windows.Forms.Label lblCliente;
        private System.Windows.Forms.Button btnTutorial;
    }
}