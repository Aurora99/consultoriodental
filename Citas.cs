﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MILIBRERIA;

namespace ConsultorioDental
{
    public partial class Citas : FormBase
    {
        public Citas()
        {
            InitializeComponent();
        }

        //public override Boolean Guardar()
        //{
            //try
            //{
                //string cmd = string.Format("EXEC ACTUALIZACIONCLIENTES '{0}','{1}','{2}','{3}'", txtBIdCliente.Text.Trim(), txtBNombre.Text.Trim(), txtBApellidos.Text.Trim(), txtBTelefono.Text.Trim());
                //UTILIDADES.Iniciar(cmd);
                //MessageBox.Show("Los datos se han guardado correctamente!..");
                //return true;
            //}
            //catch (Exception error)
            //{
                //MessageBox.Show("Ha ocurrido un error. Porfavor intente de nuevo: " + error.Message);
                //return false;
            //}
        //}
        private void Citas_Load(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void Citas_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                string cmd = string.Format("EXEC ACTUALIZACIONCLIENTES '{0}','{1}','{2}','{3}'", txtBIdCliente.Text.Trim(), txtBNombre.Text.Trim(), txtBApellidos.Text.Trim(), txtBTelefono.Text.Trim());
                UTILIDADES.Iniciar(cmd);
                MessageBox.Show("Los datos se han guardado correctamente!..");
                
            }
            catch (Exception error)
            {
                MessageBox.Show("Ha ocurrido un error. Porfavor intente de nuevo: " + error.Message);
               
            }
        }

        private void btnGuardarPS_Click(object sender, EventArgs e)
        {
            try
            {
                string cmd = string.Format("EXEC ACTUALIZACION '{0}','{1}','{2}'", txtBIDProSer.Text.Trim(), txtBProSer.Text.Trim(), txtBCosto.Text.Trim());
                UTILIDADES.Iniciar(cmd);
                MessageBox.Show("Los datos se han guardado correctamente!..");

            }
            catch (Exception error)
            {
                MessageBox.Show("Ha ocurrido un error. Porfavor intente de nuevo: " + error.Message);

            }
        }

        private void txtBIdCliente_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
