﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using MILIBRERIA;
using System.Data;


namespace ConsultorioDental
{
    public partial class Inicio : Form
    {
        public Inicio()
        {
            InitializeComponent();
        }

        public static String Codigo = "";

        private void Inicio_Load(object sender, EventArgs e)
        {
            
        }

        private void btnIniciar_Click(object sender, EventArgs e)
        {
            try
            {
                string CMD = string.Format("Select * FROM Usuario WHERE account='{0}' AND password='{1}'", txtBUsuario.Text.Trim(), txtBContraseña.Text.Trim());

                DataSet ds = UTILIDADES.Iniciar(CMD);

                Codigo = ds.Tables[0].Rows[0]["Id_Usuario"].ToString().Trim();

                string cuenta = ds.Tables[0].Rows[0]["account"].ToString().Trim();
                string contra = ds.Tables[0].Rows[0]["password"].ToString().Trim();

                if (cuenta == txtBUsuario.Text.Trim() && contra == txtBContraseña.Text.Trim())
                {
                    if(Convert.ToBoolean(ds.Tables[0].Rows[0]["Status_admin"]) == true)
                    {
                        VentanaAdministrador VenAd = new VentanaAdministrador();
                        this.Hide();
                        VenAd.Show();
                    }
                    else
                    {
                        Ventana_Cliente VenCli = new Ventana_Cliente();
                        this.Hide();
                        VenCli.Show();
                    }
                }


            }
            catch (Exception error)
            {
                MessageBox.Show("ERROR! Porfavor intente de nuevo: " + error.Message);
            }

            

        }

        private void Inicio_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void bttnSalir_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Desea salir? ", "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
            {
                this.Close();
            }
        }
    }
}
