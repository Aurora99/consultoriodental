﻿namespace ConsultorioDental
{
    partial class VentanaAdministrador
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VentanaAdministrador));
            this.btnCitas = new System.Windows.Forms.Button();
            this.btnPaginaP = new System.Windows.Forms.Button();
            this.lblTCodigo = new System.Windows.Forms.Label();
            this.lblCodigo = new System.Windows.Forms.Label();
            this.lblTUsuario = new System.Windows.Forms.Label();
            this.lblUsuario = new System.Windows.Forms.Label();
            this.lblTAdministrador = new System.Windows.Forms.Label();
            this.lblCliente = new System.Windows.Forms.Label();
            this.pBAdministrador = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pBAdministrador)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSalir
            // 
            this.btnSalir.Location = new System.Drawing.Point(435, 348);
            // 
            // btnCitas
            // 
            this.btnCitas.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCitas.Location = new System.Drawing.Point(435, 294);
            this.btnCitas.Name = "btnCitas";
            this.btnCitas.Size = new System.Drawing.Size(77, 32);
            this.btnCitas.TabIndex = 26;
            this.btnCitas.Text = "Citas";
            this.btnCitas.UseVisualStyleBackColor = true;
            this.btnCitas.Click += new System.EventHandler(this.btnCitas_Click);
            // 
            // btnPaginaP
            // 
            this.btnPaginaP.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPaginaP.Location = new System.Drawing.Point(409, 229);
            this.btnPaginaP.Name = "btnPaginaP";
            this.btnPaginaP.Size = new System.Drawing.Size(130, 36);
            this.btnPaginaP.TabIndex = 25;
            this.btnPaginaP.Text = "Pagina Principal";
            this.btnPaginaP.UseVisualStyleBackColor = true;
            this.btnPaginaP.Click += new System.EventHandler(this.btnPaginaP_Click);
            // 
            // lblTCodigo
            // 
            this.lblTCodigo.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblTCodigo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTCodigo.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTCodigo.Location = new System.Drawing.Point(151, 338);
            this.lblTCodigo.Name = "lblTCodigo";
            this.lblTCodigo.Size = new System.Drawing.Size(233, 23);
            this.lblTCodigo.TabIndex = 24;
            // 
            // lblCodigo
            // 
            this.lblCodigo.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodigo.Location = new System.Drawing.Point(23, 335);
            this.lblCodigo.Name = "lblCodigo";
            this.lblCodigo.Size = new System.Drawing.Size(62, 23);
            this.lblCodigo.TabIndex = 23;
            this.lblCodigo.Text = "Codigo:";
            // 
            // lblTUsuario
            // 
            this.lblTUsuario.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblTUsuario.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTUsuario.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTUsuario.Location = new System.Drawing.Point(151, 278);
            this.lblTUsuario.Name = "lblTUsuario";
            this.lblTUsuario.Size = new System.Drawing.Size(233, 22);
            this.lblTUsuario.TabIndex = 22;
            // 
            // lblUsuario
            // 
            this.lblUsuario.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsuario.Location = new System.Drawing.Point(23, 278);
            this.lblUsuario.Name = "lblUsuario";
            this.lblUsuario.Size = new System.Drawing.Size(70, 22);
            this.lblUsuario.TabIndex = 21;
            this.lblUsuario.Text = "Usuario:";
            // 
            // lblTAdministrador
            // 
            this.lblTAdministrador.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblTAdministrador.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTAdministrador.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTAdministrador.Location = new System.Drawing.Point(151, 207);
            this.lblTAdministrador.Name = "lblTAdministrador";
            this.lblTAdministrador.Size = new System.Drawing.Size(233, 22);
            this.lblTAdministrador.TabIndex = 20;
            // 
            // lblCliente
            // 
            this.lblCliente.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.Location = new System.Drawing.Point(23, 207);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.Size = new System.Drawing.Size(122, 22);
            this.lblCliente.TabIndex = 19;
            this.lblCliente.Text = "Administrador:";
            // 
            // pBAdministrador
            // 
            this.pBAdministrador.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pBAdministrador.BackgroundImage")));
            this.pBAdministrador.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pBAdministrador.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pBAdministrador.Location = new System.Drawing.Point(177, 23);
            this.pBAdministrador.Name = "pBAdministrador";
            this.pBAdministrador.Size = new System.Drawing.Size(207, 145);
            this.pBAdministrador.TabIndex = 28;
            this.pBAdministrador.TabStop = false;
            // 
            // VentanaAdministrador
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(634, 419);
            this.Controls.Add(this.pBAdministrador);
            this.Controls.Add(this.btnCitas);
            this.Controls.Add(this.btnPaginaP);
            this.Controls.Add(this.lblTCodigo);
            this.Controls.Add(this.lblCodigo);
            this.Controls.Add(this.lblTUsuario);
            this.Controls.Add(this.lblUsuario);
            this.Controls.Add(this.lblTAdministrador);
            this.Controls.Add(this.lblCliente);
            this.Name = "VentanaAdministrador";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "VentanaAdministrador";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.VentanaAdministrador_FormClosed);
            this.Load += new System.EventHandler(this.VentanaAdministrador_Load);
            this.Controls.SetChildIndex(this.btnSalir, 0);
            this.Controls.SetChildIndex(this.lblCliente, 0);
            this.Controls.SetChildIndex(this.lblTAdministrador, 0);
            this.Controls.SetChildIndex(this.lblUsuario, 0);
            this.Controls.SetChildIndex(this.lblTUsuario, 0);
            this.Controls.SetChildIndex(this.lblCodigo, 0);
            this.Controls.SetChildIndex(this.lblTCodigo, 0);
            this.Controls.SetChildIndex(this.btnPaginaP, 0);
            this.Controls.SetChildIndex(this.btnCitas, 0);
            this.Controls.SetChildIndex(this.pBAdministrador, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pBAdministrador)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnCitas;
        private System.Windows.Forms.Button btnPaginaP;
        private System.Windows.Forms.Label lblTCodigo;
        private System.Windows.Forms.Label lblCodigo;
        private System.Windows.Forms.Label lblTUsuario;
        private System.Windows.Forms.Label lblUsuario;
        private System.Windows.Forms.Label lblTAdministrador;
        private System.Windows.Forms.Label lblCliente;
        private System.Windows.Forms.PictureBox pBAdministrador;
    }
}