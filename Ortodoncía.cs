﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConsultorioDental
{
    public partial class Ortodoncía : Form
    {
        public Ortodoncía()
        {
            InitializeComponent();
        }

        private void btnCitas_Click(object sender, EventArgs e)
        {
            Citas citas = new Citas();
            citas.Show();
            this.Hide();
        }

        private void btnRegresar_Click(object sender, EventArgs e)
        {
            PaginaPrincipal paginaPrincipal = new PaginaPrincipal();
            paginaPrincipal.Show();
            this.Hide();
        }

        private void Ortodoncía_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void Ortodoncía_Load(object sender, EventArgs e)
        {

        }
    }
}
